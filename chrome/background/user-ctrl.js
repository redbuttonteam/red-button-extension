exports.loginProcess = loginProcess;
var currentUser = Parse.User.current();
function loginProcess (){
    if (currentUser) {
        //alert("before passowrd")
        chrome.storage.sync.get('userPassword', function(obj){
            //alert("password: " + obj.userPassword);
            loginUser(currentUser.get("username"), obj.userPassword);
        });
    } else {
        registerUser();
    }

};

function registerUser (){
    var user = new Parse.User();
    var userName = generateRandomString();
    var password = generateRandomString();
    user.set("username", userName);
    user.set("password", password);
    user.set("email", userName + "@rebutton.org");

    user.signUp(null, {
        success: function(user) {
            //alert("You have managed to sign up successfully");
            chrome.storage.sync.set({'userPassword': password}, function(){
                //alert("password saved: " + password);
            });

            //alert("before login");
            loginUser(userName, password);
        },
        error: function(user, error) {
            // Show the error message somewhere and let the user try again.
            //alert("Error sign up: " + error.code + " " + error.message);
            registerUser();
        }
    });
};

function loginUser (userName, password){
    //alert("login");
    Parse.User.logIn(userName, password, {
        success: function(user) {
            //alert("You have managed to login successfully");
            //alert("login succeeded: " + user.id);
        },
        error: function(user, error) {
            //alert("Error login: " + error.code + " " + error.message);
            if(error.code === Parse.Error.INVALID_SESSION_TOKEN){
                //alert("token is invalid section");
                Parse.User.logOut();
                loginUser(userName, password);
                //alert("logged in again");
            }
        }
    });
};

function generateRandomString(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};