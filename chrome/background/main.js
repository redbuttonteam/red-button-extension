/**
 * Created by ShaLi on 5/27/15.
 */
'use strict';
var listeners = require('./listeners');
var userCtrl = require('./user-ctrl');
chrome.runtime.onMessage.addListener(function (request,sender,sendResponse) {
    if (request.message && listeners[request.message]) {
        var listener = listeners[request.message];
        listener(request.data,function (data) {
                sendResponse({data: data});
            },
            function (error) {
                sendResponse({error: error});
            });
    }
    else {
        sendResponse({error: request.message ? 'Message Not Found' : 'Message should be specified'});
    }
    return true;
});

$(document).ready(function () {
    userCtrl.loginProcess();
    appreciationToUser();
});


function sendThanksEmail(isDistress, isViolent){
    alert("entering thanksEmail function");
    var currentUser = Parse.User.current();
    alert("current user id is = " + currentUser.id);
    var userSettings = currentUser.get("userSettings");
    alert("userSettings object = " + userSettings);
    alert("userSettings.email = " + userSettings.email);
    if(userSettings.emailInterested == true){
        alert("sending email");
        //var emailGiven = Parse.User.current().get("emailGiven");
        notifyThanksByEmail(isDistress, isViolent, userSettings.email);
    }
    else{
        alert("not sending email");
    }
}

function notifyThanksByEmail(isDistress, isViolent, emailGiven){
    Parse.Cloud.run('NotifyThanksByEmail',
        { emailAddress: emailGiven,
            distress: isDistress, violent: isViolent}).then(function(result) {
    });
}

function appreciationToUser() {
    alert("Alaram Function Entered");
    var objectId = Parse.User.current().id;
    alert("The user id is = " + objectId);
    var userReport = Parse.Object.extend("userReports");
    alert("userReport object was created");
    var query = new Parse.Query(userReport);
    query.equalTo("userName",objectId);
    query.first({
        success: function(results){
            var object = results;
            alert ("Saving the query result in a variable = " +  object);
            var prevViolent = object.get('prevViolent');
            alert ("Fetching prevViolent = " + prevViolent);
            var prevDistress = object.get('prevDistress');
            alert ("Fetching prevDistress = " + prevDistress);
            var sumOfPrevs = prevDistress + prevViolent;
            alert ("Calculating sumOfPrevs = " + sumOfPrevs);
            var violentContentCounter = object.get('violentContentCounter');
            alert ("Fetching violentContentCounter = " + violentContentCounter);
            var distressCounter = object.get('distressCounter');
            alert ("Fetching distressCounter = " + distressCounter);
            var sumOfCounters = violentContentCounter + distressCounter;
            alert ("Calculating sumOfCounters = " + sumOfCounters);
            object.set("prevViolent",violentContentCounter);
            object.save();
            object.set("prevDistress" ,distressCounter);
            object.save();
            alert ("sumOfCounters = " + sumOfCounters);
            if ((sumOfCounters > sumOfPrevs)) {
                alert("success");
                chrome.browserAction.setIcon({path: 'images/icons/notificationicon.png'});
                sendThanksNotification();
                changeNotifierEnabled(true);
                if (prevViolent < violentContentCounter) {
                    alert("Violent message");
                    changeDistressThanks(false);
                    alert("before entering thanksEmail function");
                    sendThanksEmail(false, true);
                }
                if (prevDistress < distressCounter) {
                    alert("Distress message");
                    changeDistressThanks(true);
                    sendThanksEmail(true, false);
                }
                sumOfCounters = sumOfPrevs;
            }
            else {
                alert("nothing");
            }
        },
        error: function(error) {
            alert ("Failed to fetch 'userName' from 'userReports' table : " + error.message);
        }
    });
}

function changeNotifierEnabled(isNotifierEnabled){
    localStorage.setItem("notifierEnabled", isNotifierEnabled);
    alert("NotifierEnabled HAS BEEN CHANGED TO = " + localStorage.getItem("notifierEnabled"));
}

function changeDistressThanks(isDistressThanks){
    localStorage.setItem("distressThanks", isDistressThanks);
}

function sendThanksNotification(){
    var options = {
        type: "basic",
        title: "הודעה חדשה!",
        message: "צוות הכפתור האדום מודה לך על שעזרת לנקות את האלימות מהרשת",
        iconUrl: "/images/icons/256.png"
    };
    chrome.notifications.getPermissionLevel(function(permissionLevel) {
        //alert("level = " + permissionLevel);
        if (permissionLevel == 'granted') {
            alert("notification succeeded");
            chrome.notifications.create(options, null);
        }  else if (permissionLevel == 'denied'){
            alert ("notification denied");
        }
    });

};