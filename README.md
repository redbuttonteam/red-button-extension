# The Red Button Firefox Add-on and Chrome extension

## Prerequisites
These are the tools you need to have installed on your developer machine:

* **Firefox Add-on SDK (cfx)** - See information here: [Firefox Add-on.

* **Node JS** - We need this to run our `grunt` tasks. [Download and Install here](https://nodejs.org/)

* **Grunt** - After you have node installer on your machine* you can install grunt by running the command: 

```
npm install -g grunt-cli
```
* **Bower** - After you have node installer on your machine you can install bower by running the command: 
```
npm install -g bower
```
* **Sass** - Please install the sass command line tool by following the instructions listed [here](http://sass-lang.com/install).

* **WebStorm (optional)** - You can choose whatever IDE you want. I recommend using [WebStorm](https://www.jetbrains.com/webstorm/).


## Setting up your dev environment
After you have all the prerequisites installed and you cloned the code you need to run the following commands:

* `npm install` - This will install everything needed for the grunt tasks to run.
* `bower install` - This will download all JS dependencies for the project (thinks like angular and jQuery).

## The grunt tasks
After you completed the steps mentioned above you should have an environment ready to run grunt tasks.
Here I will list the major tasks. You run grunt tasks from command line by running `grunt <task_name>`
### build
This is the main task. You run it like this: `grunt build`

This will build the chrome and firefox extensions to the build folder.

### chrome:dev and firefox:dev
This will build the appropriate extension and will listen for changed of code files.
once a code file will change it will build the extension again.