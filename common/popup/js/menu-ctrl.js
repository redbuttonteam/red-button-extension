/**
 * Created by ShaLi on 5/15/15.
 */
'use strict';
function MenuCtrl($scope, $rootScope, backgroundPage) {
    function setMenuItems() {
        $scope.menuItems = [
            {
                name: 'report',
                text: $rootScope.text.reportTab
            },
            {
                name: 'myReports',
                text: $rootScope.text.myReports
            },
            {
                name: 'settings',
                text: $rootScope.text.settings
            }
        ];
    }

    setMenuItems();
    $rootScope.$watch('text', setMenuItems);

    $rootScope.closePopup = function(){
        sendEmailGiven();
        backgroundPage.closePopup();
    };

    function sendEmailGiven(){
        var emailEntered = $('.emailGiven').val();
        //alert("emailEntered = " + emailEntered);
        if(emailEntered != null && emailEntered != ""){
            //alert("yes");
            backgroundPage.uploadEmailGiven(emailEntered);
        }
        else{
            //alert("no");
        }
    }

    $scope.setLocale = function (locale) {
        $rootScope.locale = locale;
    };
    $scope.closeNotifier = function (notifier) {
        //chrome.browserAction.setIcon({path: '../images/icons/32.png'});
        $rootScope.notifierEnabled = notifier;
        localStorage.setItem("notifierEnabled", notifier);
        alert("closing with notifier = " + notifier);
    };
}
app.controller('MenuCtrl', MenuCtrl);