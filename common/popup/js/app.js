
var app = angular.module('app', ['ui.bootstrap', 'ui.router', 'ngAudio'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/report');

        $stateProvider
            .state('report', {
                url: '/report',
                templateUrl: 'views/report.html',
                controller: 'ReportCtrl'
            })
            .state('myReports', {
                url: '/my-reports',
                templateUrl: 'views/my-reports.html',
                controller: 'MyReportsCtrl'
            })
            .state('settings', {
                url: '/settings',
                templateUrl: 'views/settings.html',
                controller: 'SettingsCtrl'
            });
    })
    .run(function ($rootScope, $http) {
        $rootScope.notifierEnabled = false;
        fetchNotifierEnabled();
        $rootScope.thanksTitle = "אנו שמחים לבשר לך";
        $rootScope.thanksSubtitleOne = "";
        $rootScope.thanksSubtitleTwo = "";
        $rootScope.text = {};
        $rootScope.locale = 'he';
        function fetchLocale(locale) {
            $http.get('locales/' + locale + '.json').success(function (data) {
                $rootScope.text = data;
            }).error(function (error) {
                $rootScope.text = {productName: error};
            });
        }

        fetchLocale($rootScope.locale);
        $rootScope.$watch('locale', fetchLocale);

        $rootScope.notifyDistressThanks = function (isDistress){
            if (isDistress){
                $rootScope.thanksSubtitleOne = "שעזרת";
                $rootScope.thanksSubtitleTwo = "לחבר במצוקה";
            } else{ // Violent
                $rootScope.thanksSubtitleOne = "שהסרת";
                $rootScope.thanksSubtitleTwo = "תוכן אלים";
            }
        };

        //$rootScope.notifyDistressThanks(false);

        function fetchNotifierEnabled(){
            var savedNotifier = localStorage.getItem("notifierEnabled");
            //alert("saved notifier = " + savedNotifier);
            if(savedNotifier == "false"){
                $rootScope.notifierEnabled = false;
                //alert("False SECTION");
            }
            else if(savedNotifier == "true"){
                $rootScope.notifierEnabled = true;
                //alert("False SECTION");
            }
            //$rootScope.notifierEnabled = savedNotifier;
        }

        function fetchDistressThanks(){
            $rootScope.notifyDistressThanks(localStorage.getItem("distressThanks"));
        }

        fetchDistressThanks();
    })
    .run(function ($rootScope, $location) {
        Parse.initialize("6xsxHmWNjN2FCi5cD8cimFmfPHZMWmEt4oWzaenH", "9IgABBu5yevvcjEsRVlg2GFVbKlr31KG9f3Q3uSS");
        $rootScope.$on('$stateChangeSuccess', function () {
            Parse.Analytics.track('view', {path: $location.path()});
        });
    });