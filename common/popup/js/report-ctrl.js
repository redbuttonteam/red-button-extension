function ReportCtrl($scope, $rootScope, backgroundPage, ngAudio) {
    $scope.sendingReport = false;
    $scope.reportData = {};
    backgroundPage.getScreenshot().then(function (screenshot) {
        $scope.reportData.screenCaptureBase64png = screenshot;
        ngAudio.play('sound/camera.mp3');
    });

    backgroundPage.getTabUrl().then(function (url) {
        $scope.reportData.uri = url;
    });

    $scope.sendReport = function () {
        $scope.reportData.isSuicidalThreat = $('#suicidalThreatCheckbox').prop('checked') ? "true" : "false";
        if($scope.reportData.isSuicidalThreat === "true"){
            $rootScope.urgentModal = true;
        }
        else {
            $scope.performSending();
        }
    };

    $scope.performSending = function () {
        $scope.closeDescriptionBox();
        if (!$('#radioSuicidalThreat').is(":checked")){
            $scope.reportData.isSuicidalThreat = false;
        }

        Parse.Analytics.track('sendReport', {withMessage: $scope.reportData.comment ? 'true' : 'false'});
        $scope.sendingReport = true;
        $scope.reportData.userID = Parse.User.current().id;
        var urgentTxt = $('#urgentTxt').val();
        $scope.reportData.distressCaseDescription = urgentTxt != undefined ? urgentTxt : "";
        backgroundPage.sendReport($scope.reportData).then(function (response) {
                $scope.response = response;
                $rootScope.successModal = true;
                $rootScope.opinionModal = false;
                $rootScope.urgentModal = false;
            },
            function (error) {
                $rootScope.generalError = error;
            }).finally(function () {
                $scope.sendingReport = false;
            });
    };

    $scope.cancelDistressCase = function (){
        $('#suicidalThreatCheckbox').attr('checked', false);
        $rootScope.urgentModal = false;
    };

    $scope.openDescriptionBox = function(){
        $('#urgentTxt').css('visibility', 'visible');
    };

    $scope.closeDescriptionBox = function(){
        $('#urgentTxt').css('visibility', 'hidden');
    }

    $scope.closeDistressDialog = function(){
        $rootScope.urgentModal = false;
    }
}
app.controller('ReportCtrl', ReportCtrl);