/**
 * Created by ShaLi on 5/30/15.
 */
'use strict';
function errorFilter($rootScope) {
    return function (error) {
        var errorMessage = error ? $rootScope.text[error] || error.message || error.responseText || error : "Empty Error";
        return errorMessage;
    };
}
app.filter('error', errorFilter);