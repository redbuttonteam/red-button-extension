/**
 * Created by ShaLi on 6/6/15.
 */
'use strict';
function MyReportsCtrl($scope, backgroundPage) {
    $scope.distressCounter = 0;
    $scope.violentContentCounter = 0;
    fetchCounters();

    $scope.openedItem = 0;
    $scope.loading = true;
    backgroundPage.getReports().then(function (reports) {
        $scope.reports = reports;

    }, function (error) {
        $scope.error = error;
    }).finally(function () {
        $scope.loading = false;
    });

    function fetchCounters(){
        var distressCounter = 0;
        var violentContentCounter = 0;
        var UserReports = Parse.Object.extend("userReports");
        var query = new Parse.Query(UserReports);
        if(Parse.User.current() != null && Parse.User.current() != undefined){
            query.equalTo("userName", Parse.User.current().id);
            query.find({
                success: function(results){
                    if(results !== undefined && results.length > 0){
                        var objectResult = results[0];
                        setCounters(objectResult.get('distressCounter'), objectResult.get('violentContentCounter'));
                    }
                },
                error: function(error){

                }
            });
        }
    }

    function setCounters(distressCounter, violentContentCounter){
        $scope.distressCounter = distressCounter;
        $scope.violentContentCounter = violentContentCounter;
    }
}
app.controller('MyReportsCtrl', MyReportsCtrl);