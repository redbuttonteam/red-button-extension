function SettingsCtrl($scope, $rootScope){
    var userSettings = {
        name : "",
        age: 0,
        place: "",
        email: "",
        emailInterested: false,
        anonymousInterested: true
    };

    $rootScope.isEmailBoxOpened = false;
    $rootScope.ArePersonalDetailsOpened = false;
    $rootScope.isSubmitButtonShown = false;

    $scope.changeAnonymousDecision = function(){
        $scope.changeIcon("anonymousDecision");
    };

    $scope.changeEmailDecision = function(){
        $scope.changeIcon("directEmailDecision");
    };

    $scope.changeIcon = function(divClassName){
        if(divClassName.includes("directEmailDecision")){
            $rootScope.isEmailBoxOpened = !$rootScope.isEmailBoxOpened;
            $rootScope.isEmailBoxOpened === true ? userSettings.emailInterested = true : userSettings.emailInterested = false;
        }
        if(divClassName.includes("anonymousDecision")){
            $rootScope.ArePersonalDetailsOpened = !$rootScope.ArePersonalDetailsOpened;
            $rootScope.ArePersonalDetailsOpened === false ? userSettings.anonymousInterested = true : userSettings.anonymousInterested = false;
        }

        var bgImage = $('.' + divClassName).css('background').replace(/^url|[\(\)]/g, '');
        if(bgImage.includes("yes-icon.png")){
            $('.' + divClassName).css('background', 'url(' + "../popup/images/no-icon.png" + ')');
            //$rootScope.isSubmitButtonShown = false;
        }
        else{
            $('.' + divClassName).css('background', 'url(' + "../popup/images/yes-icon.png" + ')');
            //$rootScope.isSubmitButtonShown = true;
        }
    };

    $scope.initSettings = function(){
        userSettings.name = $('#nameTxt').val();
        userSettings.age = $('#ageTxt').val();
        userSettings.place = $('#placeTxt').val();
        userSettings.email = $('#emailTxt').val();
    }

    $scope.sendSettings = function(){
        //alert("enter sending settings");
        $scope.initSettings();
        //alert("object settings initialized");
        var currentUser = Parse.User.current();
        currentUser.set("userSettings", userSettings);
        currentUser.save(null, {
            success: function(){
                //alert("userSettings is saved");
            },
            error: function(){
                //alert("userSettings is not saved");
            }
        });
    };
}
app.controller('SettingsCtrl', SettingsCtrl);