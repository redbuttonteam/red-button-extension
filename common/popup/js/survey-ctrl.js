function SurveyCtrl($scope, backgroundPage){
    var surveyAnswers = {
        isAnswered : false,
        helpLevel: 0,
        age: "",
        place: ""
    };
    $scope.initSurveyAnswers = function(){
        //alert("initialization entered");
        surveyAnswers.isAnswered = true;
        if($("input[type='radio'].radioBtnHelpLvl").is(':checked')) {
            var level = $("input[type='radio'].radioBtnHelpLvl:checked").val();
          //  alert(level);
            surveyAnswers.helpLevel = level;
        }
        if($("input[type='radio'].radioBtnAge").is(':checked')) {
            var age = $("input[type='radio'].radioBtnAge:checked").val();
            //alert(age);
            surveyAnswers.age = age;
        }
        if($("input[type='radio'].radioBtnPlace").is(':checked')) {
            var place = $("input[type='radio'].radioBtnPlace:checked").val();
            //alert(place);
            surveyAnswers.place = place;
        }
    };

    $scope.sendSurveyAnswers = function(){
        //alert("enter sending survey");
        $scope.initSurveyAnswers();
        //alert("object survey initialized");
        var currentUser = Parse.User.current();
        currentUser.set("surveyAnswers", surveyAnswers);
        currentUser.save(null, {
            success: function(){
                //alert("surveyAnswers is saved");
            },
            error: function(){
                //alert("surveyAnswers is not saved");
            }
        });
    };

    $scope.closeSurvey = function(){
        //alert("close survey");
        backgroundPage.closePopup();
    }
}
app.controller('SurveyCtrl', SurveyCtrl);
