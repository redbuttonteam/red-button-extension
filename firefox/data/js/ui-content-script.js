/**
 * Created by Amir Algazi on 28/03/2016.
 */

self.port.on("uiMessage", function(myAddonMessagePayload) {
    self.port.emit('showing', myAddonMessagePayload);
});

self.port.on("isSuicidalThreat", function(myAddonMessagePayload) {
    self.port.emit('showing', myAddonMessagePayload);
});