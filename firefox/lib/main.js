//var userCtrl = require('./user-ctrl');
var buttons = require('sdk/ui/button/toggle');
var panels = require("sdk/panel");
var self = require("sdk/self");
var _ = require("sdk/l10n").get;
var tabs = require("sdk/tabs");
var { setTimeout, clearTimeout } = require("sdk/timers");
//var Parse = require("./ext/parse.min");

var listeners = require('./listeners');
var button = buttons.ToggleButton({
    id: "red-button",
    label: _("productName"),
    icon: {
        "16": "./images/icons/16.png",
        "32": "./images/icons/32.png",
        "64": "./images/icons/64.png"
    },
    onChange: handleChange
});

var panel;

function handleChange(state) {
    if (state.checked) {

        //pageReady();

        const panelWidth = 350;
        panel = panels.Panel({
            contentURL: './popup/index.html',
            contentScriptFile: ['./js/popup-messaging.js', './js/ui-content-script.js'],
            width: panelWidth,
            height: 600,
            onHide: handleHide
        });

        panel.show({
            position: button
        });
// ========================================================= //
        panel.port.emit("uiMessage", "panel is showing");

        panel.port.on("showing", function(text) {
            console.error(text);
        });
        // ================================================ //

        panel.port.on('message', function (request) {
            if (request.message === 'close') {
                handleHide();
            }
            else if (request.message && listeners[request.message]) {
                var listener = listeners[request.message];

                listener(request.data, function (data) {
                        sendResponse(request.id, data);
                    },
                    function (error) {
                        sendError(request.id, error);
                    });
            }
            else {
                sendError(request.id, request.message ? 'Message Not Found' : 'Message should be specified');
            }
        })
    }
}


function sendResponse(id, data) {
    panel.port.emit('response', {id: id, data: data});
}

function sendError(id, error) {
    panel.port.emit('response', {id: id, error: error});
}

function handleHide() {
    button.state('window', {checked: false});
    panel.destroy();
}

function pageReady(){
    userCtrl.loginProcess();
    appreciationToUser();
}

function appreciationToUser() {
    //alert("Alaram Function Entered");
    var objectId = Parse.User.current().id;
    //alert("The user id is = " + objectId);
    var userReport = Parse.Object.extend("userReports");
    //alert("userReport object was created");
    var query = new Parse.Query(userReport);
    query.equalTo("userName",objectId);
    query.first({
        success: function(results){
            var object = results;
            //alert ("Saving the query result in a variable = " +  object);
            var prevViolent = object.get('prevViolent');
            //alert ("Fetching prevViolent = " + prevViolent);
            var prevDistress = object.get('prevDistress');
            //alert ("Fetching prevDistress = " + prevDistress);
            var sumOfPrevs = prevDistress + prevViolent;
            //alert ("Calculating sumOfPrevs = " + sumOfPrevs);
            var violentContentCounter = object.get('violentContentCounter');
            //alert ("Fetching violentContentCounter = " + violentContentCounter);
            var distressCounter = object.get('distressCounter');
            //alert ("Fetching distressCounter = " + distressCounter);
            var sumOfCounters = violentContentCounter + distressCounter;
            //alert ("Calculating sumOfCounters = " + sumOfCounters);
            object.set("prevViolent",violentContentCounter);
            object.save();
            object.set("prevDistress" ,distressCounter);
            object.save();
            //alert ("sumOfCounters = " + sumOfCounters);
            if ((sumOfCounters > sumOfPrevs)) {
                alert("success");
                setNotificationIcon();
                sendThanksNotification();
                //changeNotifierEnabled(true);
                if (prevViolent < violentContentCounter) {
                    //  alert("Violent message");
                    changeDistressThanks(false);
                    //alert("before entering thanksEmail function");
                    sendThanksEmail(false, true);
                }
                if (prevDistress < distressCounter) {
                    //alert("Distress message");
                    changeDistressThanks(true);
                    sendThanksEmail(true, false);
                }
                sumOfCounters = sumOfPrevs;
            }
            else {
                //alert("nothing");
            }
        },
        error: function(error) {
            //alert ("Failed to fetch 'userName' from 'userReports' table : " + error.message);
        }
    });
}

function setNotificationIcon(){
    button = buttons.ToggleButton({
        id: "red-button",
        label: _("productName"),
        icon: {
            "16": "./images/icons/notificationicon.png",
            "32": "./images/icons/notificationicon.png",
            "64": "./images/icons/notificationicon.png"
        },
        onChange: handleChange
    });
}

function changeDistressThanks(isDistressThanks){
    localStorage.setItem('distressThanks', isDistressThanks);
}

function sendThanksEmail(isDistress, isViolent){
    //alert("entering thanksEmail function");
    var currentUser = Parse.User.current();
    //alert("current user id is = " + currentUser.id);
    var userSettings = currentUser.get("userSettings");
    //alert("userSettings object = " + userSettings);
    //alert("userSettings.email = " + userSettings.email);
    if(userSettings.emailInterested == true){
        //alert("sending email");
        //var emailGiven = Parse.User.current().get("emailGiven");
        notifyThanksByEmail(isDistress, isViolent, userSettings.email);
    }
    else{
        //alert("not sending email");
    }
}

function notifyThanksByEmail(isDistress, isViolent, emailGiven){
    Parse.Cloud.run('NotifyThanksByEmail',
        { emailAddress: emailGiven,
            distress: isDistress, violent: isViolent}).then(function(result) {
    });
}

function changeNotifierEnabled(isNotifierEnabled){
    localStorage.setItem('notifierEnabled', isNotifierEnabled);
}